import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Container, NavbarBrand, Nav, NavItem, NavLink } from 'react-bootstrap';
import { ShoppingBagOutlined, AdminPanelSettingsOutlined, PersonOutlineOutlined, Logout } from '@mui/icons-material';
import UserContext, { UserProvider } from '../UserContext';
import CartSlidingPane from './CartSlidingPane';

export default function AppNavbar() {
  const { user, logout } = useContext(UserContext);
  const isAdmin = user && user.isAdmin;
  const [isPaneOpen, setIsPaneOpen] = useState(false);
  const [cartContent, setCartContent] = useState([]);
  const [error, setError] = useState(null);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/allCarts`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setCartContent(data);
        } else {
          setError('Error: API response is not an array');
        }
      })
      .catch((error) => {
        setError('Error: Unable to fetch cart content');
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleCartClick = () => {
    setIsPaneOpen((prevState) => !prevState);
  };

  return (
    <UserProvider value={{ user, logout }}>
      <div style={{ position: 'relative', zIndex: '1' }}>
        <Navbar className="navbar sticky-top" expand="lg" style={{ backgroundColor: '#ffffff' }}>
          <Container fluid={true}>
            <NavbarBrand href="/">
              <img src={process.env.PUBLIC_URL + '/images/footer-logo.png'} alt="Logo" className="img-fluid logo" />
            </NavbarBrand>

            <Navbar.Toggle aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation" />

            <Navbar.Collapse id="navbarTogglerDemo02">
              <Nav className="navbar-nav mx-auto">
                <Nav className="navbar-nav mx-auto">
                <NavItem>
                  <NavLink href="/shop">SHOP</NavLink>
                </NavItem>

                <NavItem className="px-lg-5">
                  <NavLink href="/about">ABOUT US</NavLink>
                </NavItem>

                <NavItem>
                  <NavLink href="/contact">CONTACT</NavLink>
                </NavItem>
              </Nav>
              </Nav>
              <Nav>
                <NavItem className="py-md-2 mr-lg-5">
                  {isAdmin ? (
                    <Link to="/admin">
                      <AdminPanelSettingsOutlined />
                      <span className="d-inline d-lg-none">Admin</span>
                    </Link>
                  ) : (
                    <button onClick={handleCartClick}>
                      <ShoppingBagOutlined />
                      <span className="d-inline d-lg-none">Cart</span>
                    </button>
                  )}
                </NavItem>
                 <NavItem className="py-md-2">
                  {user && user.id !== null ? (
                    <Link to="/logout">
                      <Logout />
                      <span className="d-inline d-lg-none">Logout</span>
                    </Link>
                  ) : (
                    <Link to="/login">
                      <PersonOutlineOutlined />
                      <span className="d-inline d-lg-none">Login</span>
                    </Link>
                  )}
                </NavItem>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>

      <CartSlidingPane isOpen={isPaneOpen} onRequestClose={handleCartClick} cartContent={cartContent} />
    </UserProvider>
  );
}
