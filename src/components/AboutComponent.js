import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Main() {
  return (
    <Container fluid>
      <div className="my-3 my-md-5">
        <Row className="align-items-md-center justify-content-center justify-content-lg-start">
          <Col xs={12} md={10} lg={8} className="offset-lg-3">
            <div className="about-us text-justify mt-3 mt-md-5 p-md-5">
              <h4>
                At BOOKS.com, we believe in the transformative power of books. We are an online ecommerce store dedicated to bringing you a vast collection of books from various genres and authors. Whether you're a passionate reader, a lifelong learner, or simply looking for a captivating story, we have something for everyone.
              </h4>
              <h4 className="my-3 my-md-5">
                We believe in the power of books to enlighten, entertain, and inspire. Whether you're embarking on a literary adventure, expanding your knowledge, or seeking solace in the written word, BOOKS.com is your destination. Join our vibrant community of readers, share your love for books, and embark on countless journeys through the captivating stories within our virtual shelves.
              </h4>
              <h4 className="mt-3 mt-md-5">
                Start exploring our collection today and let the magic of books unfold!
              </h4>
            </div>
          </Col>
        </Row>
      </div>
    </Container>
  );
}
