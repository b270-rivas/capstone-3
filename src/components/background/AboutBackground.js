import React from 'react';

const AboutBackground = () => {
  return (
    <div className="aboutBackground">
      <img src={process.env.PUBLIC_URL + '/images/about.gif'} alt="About" className="about-image" />
    </div>
  );
};

export default AboutBackground;