import React from 'react';

const ContactBackground = () => {
  return (
    <div className="aboutBackground">
      <img src={process.env.PUBLIC_URL + '/images/about.png'} alt="Contact" />
    </div>
  );
};

export default ContactBackground;