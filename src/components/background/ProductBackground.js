import React from 'react';

const ProductBackground = () => {
  return (
    <div className="aboutBackground">
      <img src={process.env.PUBLIC_URL + '/images/background6.png'} alt="background6" className="about-image" />
    </div>
  );
};

export default ProductBackground;