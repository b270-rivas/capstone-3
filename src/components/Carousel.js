import React from 'react';
import Container from 'react-bootstrap/Container';
import Carousel from 'react-bootstrap/Carousel';

export default function CarouselComponent() {
  return (
    <Container fluid className="p-0">
      <div className="m-2 mx-lg-5 p-0 px-lg-5">
        <Carousel className='carousel' controls={false} indicators={true} interval={3000}>
          <Carousel.Item>
            <img className='w-100 d-block' src={process.env.PUBLIC_URL + '/images/carousel1.png'} alt='carousel1' />
          </Carousel.Item>
          <Carousel.Item>
            <img className='w-100 d-block' src={process.env.PUBLIC_URL + '/images/carousel2.png'} alt='carousel2' />
          </Carousel.Item>
          <Carousel.Item>
            <img className='w-100 d-block' src={process.env.PUBLIC_URL + '/images/carousel3.png'} alt='carousel3' />
          </Carousel.Item>
          <Carousel.Item>
            <img className='w-100 d-block' src={process.env.PUBLIC_URL + '/images/carousel4.png'} alt='carousel4' />
          </Carousel.Item>
        </Carousel>
      </div>
    </Container>
  );
}