import React, { useEffect, useState } from 'react';
import { ShoppingBagOutlined } from '@mui/icons-material';
import Swal from 'sweetalert2';

const ProductRow = () => {
  // State to store the active products
  const [activeProducts, setActiveProducts] = useState([]);

  const fetchData = () => {
    // Get all active products from the database
    fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
      headers: {
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        // Update the activeProducts state with the new data
        setActiveProducts(data);
      });
  };

  const placeOrder = (productId) => {
    // Send a request to place an order for the specified productId
    fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ productId: productId })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        // Check the response to determine if the order was placed successfully
        if (data) {
          // Order placed successfully
          Swal.fire({
            icon: 'success',
            title: 'Order Placed',
            text: 'Your order has been placed successfully.',
          });
          fetchData(); // Fetch the updated data after placing the order
        } else {
          // Order placement failed
          Swal.fire({
            icon: 'error',
            title: 'Order Failed',
            text: 'Failed to place your order.',
          });
        }
      });
  };

  const addToCart = (productId) => {
    // Send a request to add the product to the cart
    fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ productId: productId })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        // Check the response to determine if the product was added to the cart successfully
        if (data) {
          // Product added to the cart successfully
          Swal.fire({
            icon: 'success',
            title: 'Product Added to Cart',
            text: 'The product has been added to your cart.',
          });
        } else {
          // Failed to add the product to the cart
          Swal.fire({
            icon: 'error',
            title: 'Failed to Add Product to Cart',
            text: 'Failed to add the product to your cart.',
          });
        }
      });
  };

  // Fetch active products on component mount and whenever the data needs to be updated
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      {/* Render the active products */}
      {activeProducts.map(product => (
        <div className="row product" key={product._id}>
          <div className="col-md-2">
            <img src={process.env.PUBLIC_URL + '/images/sample.jpg'} alt="Sample Image" height="150" />
          </div>
          <div className="col-md-8 product-detail">
            <h4>{product.name}</h4>
            <p>{product.description}</p>
            <p>Stocks: {product.stocks}</p>
            <button className="order-btn" onClick={() => placeOrder(product._id)}>Order</button> {/* Added place order button */}
            <button className="order-btn" onClick={() => addToCart(product._id)}><ShoppingBagOutlined /></button>
          </div>
          <div className="col-md-2 product-price">
            ${product.price}
          </div>
        </div>
      ))}
    </div>
  );
}

export default ProductRow;