import React, { useContext, useState, useEffect } from 'react';
import SlidingPane from 'react-sliding-pane';
import 'react-sliding-pane/dist/react-sliding-pane.css';
import UserContext from '../UserContext';

export default function CartSlidingPane({ isOpen, onRequestClose }) {
  const { user } = useContext(UserContext);
  const isAdmin = user && user.isAdmin;
  const [cartContent, setCartContent] = useState([]);
  const [error, setError] = useState(null);

  const fetchData = async () => {
    try {
      const res = await fetch(`${process.env.REACT_APP_API_URL}/users/allCarts`, {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${localStorage.getItem('token')}`,
        },
      });

      let data = await res.json();

      console.log("Cart Content:", data);

      if (Array.isArray(data)) {
        setCartContent(data);
      } else {
        setError({ message: 'Error: API response is not an array' });
      }
    } catch (error) {
      setError({ message: 'Error: Failed to fetch cart content' });
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <SlidingPane
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      width="300px"
      className="sliding-pane"
      overlayClassName="sliding-pane-overlay"
    >
      <h2>Cart</h2>
      {/* Display the fetched cart content */}
      {cartContent.map((item) => (
        <div key={item._id}>
          <p>Product ID: {item.productId}</p>
          <p>Added On: {item.addedOn}</p>
          <p>Status: {item.status}</p>
        </div>
      ))}
      {isAdmin && <p>Admin-specific content</p>}
      {error && <p>{error.message}</p>}
    </SlidingPane>
  );
}