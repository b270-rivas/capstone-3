import React, { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Nav, Tab, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { MDBInput, MDBCheckbox, MDBTabsPane, MDBIcon } from 'mdb-react-ui-kit';

export default function RegisterComponent() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  // State hooks to store the values of the input fields
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [passwordMatch, setPasswordMatch] = useState(true); // State hook to track password matching
  // Set to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      mobileNo.length === 11 &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
      setPasswordMatch(true); // Update password matching state
    } else {
      setIsActive(false);
      setPasswordMatch(false); // Update password matching state
    }
  }, [firstName, lastName, email, mobileNo, password1, password2]);

  function registerUser(e) {
  e.preventDefault();

  // Check duplicate email
  fetch(`${process.env.REACT_APP_API_URL}/users/details?email=${email}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);

      if (data.length > 0) {
          Swal.fire({
            title: 'Duplicate email found',
            icon: 'error',
            text: 'Kindly provide another email to complete the registration.',
          });
        } else {

          // Proceed registration
          fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then(res => res.json())
            .then(data => {

                console.log(data);

              if (data) {
                // Clear input fields
                setFirstName('');
                setLastName('');
                setEmail('');
                setMobileNo('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                  title: 'Registration successful',
                  icon: 'success',
                  text: 'Welcome to Zuitt!',
                });

                // Allows us to redirect the user to the login page after registering for an account
                navigate('/login');
              } else {
                Swal.fire({
                  title: 'Something went wrong.',
                  icon: 'error',
                  text: 'Please try again.',
                });
              }
            })
            .catch((error) => {
              Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'An error occurred while registering.',
              });
              console.error(error);
            });
        }
      })
      .catch((error) => {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred while checking email availability.',
        });
        console.error(error);
      });
  }

  const [justifyActive, setJustifyActive] = useState('tab2');

  const handleJustifyClick = (value) => {
    if (value === justifyActive) {
      return;
    }

    if (value === 'tab1') {
      navigate('/login');
    } else {
      setJustifyActive(value);
    }
  };


  return (
    (user.id !== null) 
    ?
        <Navigate to="/shop" />
    :
    <div className="container-fluid d-flex justify-content-center align-items-center vh-100">
      <div className="background">
        <img src={process.env.PUBLIC_URL + '/images/background1.png'} alt="Background1" className="background-login" />
      </div>
      <div className="login col-12 col-md-8 col-lg-6">
        <Nav variant="pills" justify className="mb-3 d-flex flex-row justify-content-between mt-4">
          <Nav.Item>
            <Nav.Link
              onClick={() => handleJustifyClick('tab1')}
              active={justifyActive === 'tab1'}
              className={justifyActive === 'tab1' ? 'custom-active-link' : 'custom-link'}
            >
              LOGIN
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link
              onClick={() => handleJustifyClick('tab2')}
              active={justifyActive === 'tab2'}
              className={justifyActive === 'tab2' ? 'custom-active-link' : 'custom-link'}
            >
              REGISTER
            </Nav.Link>
          </Nav.Item>
        </Nav>

        <Tab.Content>
          <MDBTabsPane show={justifyActive === 'tab2'}>
          	<div className="text-center mb-3">
              <p className="login-p">Sign up with:</p>

              <div className="button-group d-flex justify-content-between mx-auto" style={{ maxWidth: '300px' }}>
                <Button tag="a" color="none" className="m-1">
                  <MDBIcon fab icon="facebook-f" size="sm" />
                </Button>

                <Button tag="a" color="none" className="m-1">
                  <MDBIcon fab icon="twitter" size="sm" />
                </Button>

                <Button tag="a" color="none" className="m-1">
                  <MDBIcon fab icon="google" size="sm" />
                </Button>

                <Button tag="a" color="none" className="m-1">
                  <MDBIcon fab icon="github" size="sm" />
                </Button>
              </div>

              <p className="text-center mt-3">or:</p>
            </div>

            <div className="text-center mb-3">
              <p className="login-p">Sign up with:</p>
              {/* ... */}
            </div>

            <MDBInput
              wrapperClass="mb-4"
              placeholder="First Name"
              id="register-form-firstname"
              type="text"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
            <MDBInput
              wrapperClass="mb-4"
              placeholder="Last Name"
              id="register-form-lastname"
              type="text"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
            <MDBInput
              wrapperClass="mb-4"
              placeholder="Email"
              id="register-form-email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <MDBInput
              wrapperClass="mb-4"
              placeholder="Mobile Number"
              id="register-form-mobile"
              type="text"
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
            />
            <MDBInput
              wrapperClass="mb-4"
              placeholder="Password"
              id="register-form-password1"
              type="password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
            />

            {!passwordMatch && <p className="password-mismatch-text my-0">Passwords do not match</p>} {/* Display error message if passwords don't match */}
            <MDBInput
              wrapperClass="mb-4"
              placeholder="Confirm Password"
              id="register-form-password2"
              type="password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
              className={!passwordMatch ? 'password-mismatch' : ''} // Apply CSS class if passwords don't match
            />
            
            <div className="login-p d-flex justify-content-center mb-4">
              <MDBCheckbox name="flexCheck" id="register-form-flexCheckDefault" label="I have read and agree to the terms" />
            </div>

            <Button className="button mb-4 w-100 custom-btn" disabled={!isActive} onClick={registerUser}>
              Sign up
            </Button>
          </MDBTabsPane>
        </Tab.Content>
      </div>
    </div>
  );
}