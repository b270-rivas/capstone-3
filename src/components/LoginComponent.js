import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { Nav, Tab, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { MDBTabsPane, MDBIcon, MDBInput, MDBCheckbox } from 'mdb-react-ui-kit';

export default function LoginComponent() {
  const { setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const retrieveUserDetails = (token) => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      });
  };

  const authenticate = (e) => {
    e.preventDefault();

    fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if(data.accessToken !== undefined) {

            // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
            localStorage.setItem('token', data.accessToken);
            retrieveUserDetails(data.accessToken);

            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Books!"
            })

            navigate('/');
            
        } else {
            Swal.fire({
                title: "Authentication failed!",
                icon: "error",
                text: "Check your log in credentials and try again!"
            })
        }
      });

    setEmail('');
    setPassword('');
  };

  const [justifyActive, setJustifyActive] = useState('tab1');

  const handleJustifyClick = (value) => {
    if (value === justifyActive) {
      return;
    }

    if (value === 'tab2') {
      navigate('/register');
    } else {
      setJustifyActive(value);
    }
  };

  const [isActive, setIsActive] = useState(true);
  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <div className="container-fluid d-flex justify-content-center align-items-center vh-100">
      <div className="background">
        <img src={process.env.PUBLIC_URL + '/images/background1.png'} alt="Background1" className="background-login" />
      </div>
      <div className="login col-12 col-md-8 col-lg-6">
        <Nav variant="pills" justify className="mb-3 d-flex flex-row justify-content-between mt-4">
          <Nav.Item>
            <Nav.Link
              onClick={() => handleJustifyClick('tab1')}
              active={justifyActive === 'tab1'}
              className={justifyActive === 'tab1' ? 'custom-active-link' : 'custom-link'}
            >
              LOGIN
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link
              onClick={() => handleJustifyClick('tab2')}
              active={justifyActive === 'tab2'}
              className={justifyActive === 'tab2' ? 'custom-active-link' : 'custom-link'}
            >
              REGISTER
            </Nav.Link>
          </Nav.Item>
        </Nav>

        <Tab.Content>
          <MDBTabsPane show={justifyActive === 'tab1'}>
            <div className="text-center mb-3">
              <p className="login-p">Sign in with:</p>

              <div className="button-group d-flex justify-content-between mx-auto" style={{ maxWidth: '300px' }}>
                <button className="btn btn-link m-1">
                  <MDBIcon fab icon="facebook-f" size="sm" />
                </button>

                <button className="btn btn-link m-1">
                  <MDBIcon fab icon="twitter" size="sm" />
                </button>

                <button className="btn btn-link m-1">
                  <MDBIcon fab icon="google" size="sm" />
                </button>

                <button className="btn btn-link m-1">
                  <MDBIcon fab icon="github" size="sm" />
                </button>
              </div>

              <p className="login-p text-center mt-3">or:</p>
            </div>

            <MDBInput
              wrapperClass="mb-4"
              placeholder="Email address"
              id="login-form-email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <MDBInput
              wrapperClass="mb-4"
              placeholder="Password"
              id="login-form-password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />

            <div className="d-flex justify-content-between flex-wrap mx-4 mb-4">
              <div className="login-p d-flex align-items-center">
                <MDBCheckbox name="flexCheck" value="" id="login-form-flexCheckDefault" label="Remember me" />
              </div>
              <div className="login-p mt-2 mt-sm-0">
                <a className="forgot" href="!#">
                  Forgot password?
                </a>
              </div>
            </div>

            <Button className="button mb-4 w-100 custom-btn" disabled={!isActive} onClick={authenticate}>
              Sign in
            </Button>
          </MDBTabsPane>
        </Tab.Content>
      </div>
    </div>
  );
}