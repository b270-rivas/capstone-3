import React, { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import AppNavbar from '../components/AppNavbar';
import Background from '../components/background/Background';
import { Button, Table } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";


export default function ShowOrders() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [allOrders, setAllOrders] = useState([]);

  const fetchOrders = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      if (response.ok) {
        const data = await response.json();
        setAllOrders(data);
      } else {
        throw new Error("Failed to fetch orders");
      }
    } catch (error) {
      console.error(error);
      // Handle error accordingly (e.g., show an error message)
    }
  };

  useEffect(() => {
    fetchOrders();
  }, []);

  return (
    user.isAdmin ? (
      <>
        <AppNavbar />
        <div className="background">
          <img src={process.env.PUBLIC_URL + '/images/background4.jpg'} alt="Background1" className="background-login" />
        </div>
        {/* Header for the admin dashboard and functionality for create product and show order */}
        <div className="mt-5 mb-3 text-center">
          <h1>All Orders</h1>
        </div>
        {/* End of admin dashboard header */}
        {/* For view all the products in the database. */}
        <div className="tableContainer">
          <Table className="adminTable col-12" striped bordered hover>
            <thead>
              <tr>
                <th>Product ID</th>
                <th>Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {allOrders.map(order => (
                <tr key={order.productId}>
                  <td>{order.productId}</td>
                  <td>{order.ordersOn}</td>
                  <td>{order.status}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        {/* End of table for product viewing */}
      </>
    ) : (
      <Navigate to="/shop" />
    )
  );
}
