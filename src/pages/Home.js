import React from 'react';
import AppNavbar from '../components/AppNavbar';
import Background from '../components/background/Background';
import Main from '../components/Main';
import Collection from '../components/Collection';
import Carousel from '../components/Carousel';
import Subfooter from '../components/Subfooter';
import Footer from '../components/Footer';

export default function Home() {
    return (
        <div>
            <AppNavbar />
            <Background />
            <Main />
            <Collection />
            <Carousel />
            <Subfooter />
            <Footer />
        </div>
    );
}