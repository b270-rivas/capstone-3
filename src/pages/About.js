import React from 'react';
import AboutBackground from '../components/background/AboutBackground';
import AppNavbar from '../components/AppNavbar';
import AboutComponent from '../components/AboutComponent';
import Footer from '../components/Footer';
import Container from 'react-bootstrap/Container';

export default function About() {
    return (
        <Container fluid="true">
            <div>
                <AboutBackground />
                <AppNavbar />
                <AboutComponent />
                <Footer />
            </div>
        </Container>
    );
}