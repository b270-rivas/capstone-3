import React, { Component } from 'react';
import AppNavbar from '../components/AppNavbar';
import ProductBackground from '../components/background/ProductBackground';
import ProductRow from '../components/ProductRow';

class ProductList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
      	<AppNavbar />
      	<ProductBackground />
      	<div className="container main-content">
      	  <ProductRow />
      	</div>
      </div>
    );
  }
}

export default ProductList;