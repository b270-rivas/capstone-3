import React from 'react';
import ContactBackground from '../components/background/ContactBackground';
import AppNavbar from '../components/AppNavbar';
import Subfooter from '../components/Subfooter';
import Footer from '../components/Footer';

export default function Contact() {
    return (
        <div>
            <ContactBackground />
            <AppNavbar />
            <div className="container-fluid">
                <div className="row align-items-center justify-content-center my-lg-5 py-lg-5" id="contact">

                     <div className="col-12 col-md-4 order-1 order-md-1 p-0">
                        <div className="map-container">
                            <iframe
                                title="Contact Location"
                                className="map-iframe"
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125585.1679728078!2d123.69040144736958!3d10.378895746997932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a999258dcd2dfd%3A0x4c34030cdbd33507!2sCebu%20City%2C%20Cebu!5e0!3m2!1sen!2sph!4v1688452356810!5m2!1sen!2sph"
                                allowFullScreen
                                loading="lazy"
                            />
                        </div>
                    </div>

                    <div className="col-12 col-md-6 order-2 order-md-2 mb-3 mb-md-4 p-0">
                        <h1 className="text-dark text-center mb-3 my-lg-0" id="h1-fluid">Get in touch with us!</h1>
                        <form className="pl-lg-5 rounded" id="form">
                            <div className="form-group">
                                <label className="text-light" htmlFor="name">Name</label>
                                <input type="text" className="form-control" id="name" placeholder="Enter Name" />
                            </div>

                            <div className="form-group">
                                <label className="text-light" htmlFor="email">Email address</label>
                                <input type="email" className="form-control" id="email" placeholder="name@example.com" />
                            </div>

                            <div className="form-group">
                                <label className="text-light" htmlFor="contact">Contact Number</label>
                                <input type="text" className="form-control" id="contact" placeholder="+63 900 000 0000" />
                            </div>

                            <div className="form-group">
                                <label className="text-light" htmlFor="message">Message</label>
                                <textarea className="form-control" id="message" rows="7" placeholder="Write a Message"></textarea>
                            </div>

                            <button type="button" className="btn btn-dark" data-toggle="modal" data-target="#exampleModal">Send Email</button>

                            <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h6 className="modal-title" id="exampleModalLabel">Thank you for contacting us!</h6>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div className="modal-body">Message Successfully Sent</div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <Subfooter />
            <Footer />
        </div>
    );
}